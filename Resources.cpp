#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include "Resources.hpp"

Resources::Resources(){
	Textures = new sf::Texture[TEXTURE_RESOURCE_COUNT];
	
	for(int I = 0;I < TEXTURE_RESOURCE_COUNT;++I){
		switch(I){
			case IRV_NAME_TEXTURE:
				Textures[I].loadFromFile("Textures/IrvName.png");
				break;
			case TILES_TEXTURE_ATLAS:
				Textures[I].loadFromFile("Textures/Tiles.png");
				break;
			case BACKGROUND_TILE_TEXTURE:
				Textures[I].loadFromFile("Textures/Background.png");
				Textures[I].setRepeated(true);
				break;
			case UP_BUTTON_TEXTURE:
				Textures[I].loadFromFile("Textures/Buttons/Up.png");
				break;
			case DOWN_BUTTON_TEXTURE:
				Textures[I].loadFromFile("Textures/Buttons/Down.png");
				break;
			case RIGHT_BUTTON_TEXTURE:
				Textures[I].loadFromFile("Textures/Buttons/Right.png");
				break;
			case LEFT_BUTTON_TEXTURE:
				Textures[I].loadFromFile("Textures/Buttons/Left.png");
				break;
			case DOTHING_BUTTON_TEXTURE:
				Textures[I].loadFromFile("Textures/Buttons/DoThing.png");
				break;
			case CONTINUE_STORY_BUTTON_TEXTURE:
				Textures[I].loadFromFile("Textures/Buttons/Story.png");
				break;
			case NEW_STORY_BUTTON_TEXTURE:
				Textures[I].loadFromFile("Textures/Buttons/New.png");
				break;
			case NEW_SURVIVAL_BUTTON_TEXTURE:
				Textures[I].loadFromFile("Textures/Buttons/Survival.png");
				break;
			case SURVIVAL_HIGHS_BUTTON_TEXTURE:
				Textures[I].loadFromFile("Textures/Buttons/Highscores.png");
				break;
			case MUTE_MUSIC_BUTTON_TEXTURE:
				Textures[I].loadFromFile("Textures/Buttons/Music.png");
				break;
			case MUTE_AUDIO_BUTTON_TEXTURE:
				Textures[I].loadFromFile("Textures/Buttons/Sounds.png");
				break;
			case PONKA_TEXTURE:
				Textures[I].loadFromFile("Textures/Ponka.png");
				break;
		}
	}
	
	Fonts = new sf::Font[FONT_RESOURCE_COUNT];
	
	for(int I = 0;I < FONT_RESOURCE_COUNT;++I){
		switch(I){
			case PUCEPERISHED_FONT:
				Fonts[I].loadFromFile("Fonts/PucePerished.ttf");
				break;
		}
	}
	
	SoundBuffers = new sf::SoundBuffer[SOUND_BUFFER_RESOURCE_COUNT];
	
	for(int I = 0;I < SOUND_BUFFER_RESOURCE_COUNT;++I){
		switch(I){
			case DERP_AUDIO:
				break;
		}
	}
}

sf::Texture *Resources::RetrieveTexture(TextureResource RequestedResource){
	return &Textures[RequestedResource];
}

sf::Font *Resources::RetrieveFont(FontResource RequestedResource){
	return &Fonts[RequestedResource];
}

sf::SoundBuffer *Resources::RetrieveSoundBuffer(SoundBufferResource RequestedResource){
	return &SoundBuffers[RequestedResource];
}