#ifndef RESOURCE_MANAGER_INC
	#include <SFML/Graphics.hpp>
	#include <SFML/Audio.hpp>
	
	enum TextureResource{
		IRV_NAME_TEXTURE,
		
		TILES_TEXTURE_ATLAS,
		BACKGROUND_TILE_TEXTURE,
		
		UP_BUTTON_TEXTURE,
		DOWN_BUTTON_TEXTURE,
		RIGHT_BUTTON_TEXTURE,
		LEFT_BUTTON_TEXTURE,
		
		DOTHING_BUTTON_TEXTURE,
		
		CONTINUE_STORY_BUTTON_TEXTURE,
		NEW_STORY_BUTTON_TEXTURE,
		
		NEW_SURVIVAL_BUTTON_TEXTURE,
		SURVIVAL_HIGHS_BUTTON_TEXTURE,
		
		MUTE_MUSIC_BUTTON_TEXTURE,
		MUTE_AUDIO_BUTTON_TEXTURE,
		
		PONKA_TEXTURE,
		
		TEXTURE_RESOURCE_COUNT
	};
	
	enum FontResource{
		PUCEPERISHED_FONT,
		
		FONT_RESOURCE_COUNT
	};
	
	enum SoundBufferResource{
		DERP_AUDIO,
		
		SOUND_BUFFER_RESOURCE_COUNT
	};
	
	class Resources{
		private:
			sf::Texture *Textures;
			sf::Font *Fonts;
			sf::SoundBuffer *SoundBuffers;
			
		public:
			Resources();
			
			sf::Texture *RetrieveTexture(TextureResource RequestedResource);
			sf::Font *RetrieveFont(FontResource RequestedResource);
			sf::SoundBuffer *RetrieveSoundBuffer(SoundBufferResource RequestedResource);
	};
	
	#define RESOURCE_MANAGER_INC
#endif