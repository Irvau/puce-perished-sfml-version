#include <cmath>
#include <string>

#include <SFML/Graphics.hpp>

#include "TileMap.hpp"

void TileMap::draw(sf::RenderTarget& Target,sf::RenderStates States) const{
	States.transform *= getTransform();
	States.texture = Texture;
	
	Target.draw(Vertices,States);
}

TileMap::TileMap(sf::Vector2u NewDimensions,sf::Texture *NewTexture,std::vector<int> *NewTiles)
:Dimensions(NewDimensions){
	SetTexture(NewTexture);
	
	Vertices.setPrimitiveType(sf::Quads);
	SetTiles(NewTiles);
}

void TileMap::SetDimensions(sf::Vector2u NewDimensions){
	Dimensions = NewDimensions;
}

void TileMap::SetTexture(sf::Texture *NewTexture){
	if(NewTexture){
		Texture = NewTexture;
	}
}

bool TileMap::SetTiles(std::vector<int> *NewTiles){
	if(NewTiles){
		Tiles = *NewTiles;
		
		unsigned int TileCount = Dimensions.x * Dimensions.y;
		
		Vertices.resize(TileCount * 4);
		
		for(unsigned int I = 0;I < TileCount && I < Tiles.size();++I){
			sf::Vertex *Quad = &Vertices[I * 4];
			
			Quad[0].position = sf::Vector2f(std::floor(I % Dimensions.x) * TILE_SIZE.x,std::floor(I / Dimensions.x) * TILE_SIZE.y);
			Quad[1].position = sf::Vector2f(std::floor((I % Dimensions.x) + 1) * TILE_SIZE.x,std::floor(I / Dimensions.x)  * TILE_SIZE.y);
			Quad[2].position = sf::Vector2f(std::floor((I % Dimensions.x) + 1) * TILE_SIZE.x,std::floor((I / Dimensions.x) + 1) * TILE_SIZE.y);
			Quad[3].position = sf::Vector2f(std::floor(I % Dimensions.x) * TILE_SIZE.x,std::floor((I / Dimensions.x) + 1) * TILE_SIZE.y);
			
			if(Texture){
				unsigned int TextureTileWidth = std::floor(Texture->getSize().x / TILE_TEXTURE_SIZE.x);
				
				Quad[0].texCoords = sf::Vector2f(std::floor(Tiles[I] % TextureTileWidth) * TILE_TEXTURE_SIZE.x,std::floor(Tiles[I] / TextureTileWidth) * TILE_TEXTURE_SIZE.y);
				Quad[1].texCoords = sf::Vector2f(std::floor((Tiles[I] % TextureTileWidth) + 1) * TILE_TEXTURE_SIZE.x,std::floor(Tiles[I] / TextureTileWidth) * TILE_TEXTURE_SIZE.y);
				Quad[2].texCoords = sf::Vector2f(std::floor((Tiles[I] % TextureTileWidth) + 1) * TILE_TEXTURE_SIZE.x,std::floor((Tiles[I] / TextureTileWidth) + 1) * TILE_TEXTURE_SIZE.y);
				Quad[3].texCoords = sf::Vector2f(std::floor(Tiles[I] % TextureTileWidth) * TILE_TEXTURE_SIZE.x,std::floor((Tiles[I] / TextureTileWidth) + 1) * TILE_TEXTURE_SIZE.y);
			}
		}
		
		return true;
	}
	
	return false;
}