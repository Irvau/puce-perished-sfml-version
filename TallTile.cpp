#include <cmath>

#include "Game.hpp"
#include "TallTile.hpp"
#include "TileMap.hpp"

void TallTile::Update(){
	SetAlpha(GetAlpha() + ((Fade * -2) + 1) * 0.6 * Game::PP.DeltaTime.asSeconds());
	SetAlpha(GetAlpha() - (GetAlpha() < 0.3) * (GetAlpha() - 0.3));
	Fade = false;
}

void TallTile::draw(sf::RenderTarget& Target,sf::RenderStates States) const{
	if(Texture){
		States.transform *= getTransform();
		States.texture = Texture;
		
		Target.draw(Vertices,States);
	}
}

TallTile::TallTile(int TileID,sf::Vector2i NewPosition,sf::Texture *NewTexture)
:Entity(TALL_TILE_ENTITY,sf::Vector2f(NewPosition.x * TILE_SIZE.x,NewPosition.y * TILE_SIZE.y)),Texture(NewTexture),Alpha(1.0),Fade(false){
	Vertices.setPrimitiveType(sf::Quads);
	Vertices.resize(8);
	
	Vertices[0].position = sf::Vector2f(0,TILE_SIZE.y);
	Vertices[1].position = sf::Vector2f(TILE_SIZE.x,TILE_SIZE.y);
	Vertices[2].position = sf::Vector2f(TILE_SIZE.x,TILE_SIZE.y - TILE_SIZE.x);
	Vertices[3].position = sf::Vector2f(0,TILE_SIZE.y - TILE_SIZE.x);
	
	Vertices[4].position = sf::Vector2f(0,TILE_SIZE.y - TILE_SIZE.x);
	Vertices[5].position = sf::Vector2f(TILE_SIZE.x,TILE_SIZE.y - TILE_SIZE.x);
	Vertices[6].position = sf::Vector2f(TILE_SIZE.x,TILE_SIZE.x * -1);
	Vertices[7].position = sf::Vector2f(0,TILE_SIZE.x * -1);
	
	SetTileID(TileID);
}

bool TallTile::SetTileID(int NewID){
	if(Texture){
		unsigned int TextureTileWidth = std::floor(Texture->getSize().x / TILE_TEXTURE_SIZE.x);
		
		Vertices[0].texCoords = sf::Vector2f(std::floor(NewID % TextureTileWidth) * TILE_TEXTURE_SIZE.x,std::floor((NewID / TextureTileWidth) + 2) * TILE_TEXTURE_SIZE.y);
		Vertices[1].texCoords = sf::Vector2f(std::floor((NewID % TextureTileWidth) + 1) * TILE_TEXTURE_SIZE.x,std::floor((NewID / TextureTileWidth) + 2) * TILE_TEXTURE_SIZE.y);
		Vertices[2].texCoords = sf::Vector2f(std::floor((NewID % TextureTileWidth) + 1) * TILE_TEXTURE_SIZE.x,std::floor((NewID / TextureTileWidth) + 1) * TILE_TEXTURE_SIZE.y);
		Vertices[3].texCoords = sf::Vector2f(std::floor(NewID % TextureTileWidth) * TILE_TEXTURE_SIZE.x,std::floor((NewID / TextureTileWidth) + 1) * TILE_TEXTURE_SIZE.y);
		
		Vertices[4].texCoords = sf::Vector2f(std::floor(NewID % TextureTileWidth) * TILE_TEXTURE_SIZE.x,std::floor((NewID / TextureTileWidth) + 1) * TILE_TEXTURE_SIZE.y);
		Vertices[5].texCoords = sf::Vector2f(std::floor((NewID % TextureTileWidth) + 1) * TILE_TEXTURE_SIZE.x,std::floor((NewID / TextureTileWidth) + 1) * TILE_TEXTURE_SIZE.y);
		Vertices[6].texCoords = sf::Vector2f(std::floor((NewID % TextureTileWidth) + 1) * TILE_TEXTURE_SIZE.x,std::floor(NewID / TextureTileWidth) * TILE_TEXTURE_SIZE.y);
		Vertices[7].texCoords = sf::Vector2f(std::floor(NewID % TextureTileWidth) * TILE_TEXTURE_SIZE.x,std::floor(NewID / TextureTileWidth) * TILE_TEXTURE_SIZE.y);
		
		return true;
	}
	
	return false;
}

void TallTile::Position(sf::Vector2i NewPosition){
	setPosition(NewPosition.x * TILE_SIZE.x,NewPosition.y * TILE_SIZE.y);
}

void TallTile::SetTexture(sf::Texture *NewTexture){
	Texture = NewTexture;
}

void TallTile::SetAlpha(float NewAlpha){
	Alpha = NewAlpha;
	Alpha += ((Alpha > 1) * (1 - Alpha)) + ((Alpha < 0) * (0 - Alpha));
	
	for(int I = 0;I < 8;++I){
		Vertices[I].color = sf::Color(255,255,255,255 * Alpha);
	}
}

float TallTile::GetAlpha(){
	return Alpha;
}

sf::Vector2u TallTile::GetDimensions() const{
	return sf::Vector2u(TILE_SIZE.x,TILE_SIZE.y);
}