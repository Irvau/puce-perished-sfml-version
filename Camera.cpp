#include <algorithm>
#include <cmath>

#include "Game.hpp"
#include "Resources.hpp"
#include "Camera.hpp"
#include "Button.hpp"

void Camera::draw(sf::RenderTarget& RTarget,sf::RenderStates States) const{
	RTarget.setView(View);
	
	if(Background.getTexture()){
		RTarget.draw(Background);
	}
	
	if(TargetScene){
		RTarget.draw(*TargetScene);
	}
	
	RTarget.setView(RTarget.getDefaultView());
}

Camera::Camera(sf::Vector2u NewDimensions,Scene *NewTargetScene,int NewID,sf::Vector2f InitPosition,Inputs *NewInput)
:TargetScene(NewTargetScene),ID(NewID),Target(0){
	sf::Vector2f PossibleScalage(1.0,1.0);
	
	PossibleScalage.x += ((Game::PP.ViewDimensions.x / NewDimensions.x) > 1.0) * ((Game::PP.ViewDimensions.x / NewDimensions.x) - 1);
	PossibleScalage.y += ((Game::PP.ViewDimensions.y / NewDimensions.y) > 1.0) * ((Game::PP.ViewDimensions.y / NewDimensions.y) - 1);
	float MaxScalage = std::max(PossibleScalage.x,PossibleScalage.y);
	
	OriginalSize = sf::Vector2f(NewDimensions.x * MaxScalage,NewDimensions.y * MaxScalage);
	View.reset(sf::FloatRect(0,0,OriginalSize.x,OriginalSize.y));
	
	View.setCenter(InitPosition);
	
	//Background.setTexture(*(Game::PP.Stuffs->RetrieveTexture(BACKGROUND_TILE_TEXTURE)));
	//Background.setTextureRect(sf::IntRect(0,0,View.getSize().x,View.getSize().y));
	
	if(NewInput){
		Input = NewInput;
	}else{
		Input = new Inputs;
	}
}

void Camera::Update(){
	TargetScene->Input.CopyFrom(Input);
	
	if(Target){
		Sprite *TruTarget = Target;
		
		if(Target->GetSpriteType() == BUTTON_SPRITE){
			Button *TruButton = static_cast<Button*>(Target);
			TruTarget = TruButton->Target;
			
			if(!OldTarget->IsTouching(Target)){
				Target = OldTarget;
				OldTarget = 0;
			}
		}else{
			for(std::list<Entity*>::iterator IT = TargetScene->Entities.begin();IT != TargetScene->Entities.end();++IT){
				if((*IT)->GetEntityType() == SPRITE_ENTITY){
					Sprite *PossSprite = static_cast<Sprite*>(*IT);
					
					if(PossSprite->GetSpriteType() == BUTTON_SPRITE && Target->IsTouching(PossSprite)){
						Button *PossButton = static_cast<Button*>(PossSprite);
						
						if(PossButton->Target){
							OldTarget = Target;
							Target = PossButton;
						}
					}
				}
			}
		}
		
		Center = sf::Vector2f(
			Center.x - ((Center.x - TruTarget->getPosition().x - (TruTarget->GetDimensions().x / 2)) * (0.5 * Game::PP.DeltaTime.asSeconds())),
			Center.y - ((Center.y - TruTarget->getPosition().y - (TruTarget->GetDimensions().y / 2)) * (0.5 * Game::PP.DeltaTime.asSeconds()))

		);
		
		View.setCenter((int)Center.x,(int)Center.y);
	}
	
	Background.setPosition(View.getCenter() - sf::Vector2f(View.getSize().x / 2,View.getSize().y / 2));
}

void Camera::PointTo(sf::Vector2f NewCenter){
	View.setCenter(NewCenter);
}

void Camera::SetTarget(Sprite *NewTarget){
	Target = NewTarget;
}

sf::Vector2f Camera::GetCoords(){
	return View.getCenter();
}