#ifndef PINKAMENA_INC
	#include <list>
	
	#include "Game.hpp"
	#include "TallTile.hpp"
	#include "Sprite.hpp"
	
	class Pinkamena: public Sprite{
		friend class Scene;
		
		private:
			bool Flipped;
			
			virtual void Update();

		public:
			Pinkamena(sf::Vector2f InitPosition = sf::Vector2f(0,0));
			
			virtual sf::Vector2u GetDimensions() const;
	};
	
	#define PINKAMENA_INC
#endif