#ifndef CAMERA_INC
	#include <SFML/Graphics.hpp>
	
	#include "InputStuffs.hpp"
	#include "Scene.hpp"
	#include "Sprite.hpp"
	
	class Camera: public sf::Transformable, public sf::Drawable{
		friend class Game;
		friend class Scene;
		friend class CameraMonitor;
		
		private:
			Scene *TargetScene;
			Inputs *Input;
			
			int ID;
			
			sf::View View;
			sf::Sprite Background;
			
			Sprite *Target,*OldTarget;
			sf::Vector2f Center;
			
			sf::Vector2f OriginalSize;
			
			virtual void draw(sf::RenderTarget& RTarget,sf::RenderStates States) const;
			
		public:
			Camera(sf::Vector2u NewDimensions,Scene *NewTargetScene = 0,int NewID = 0,sf::Vector2f InitPosition = sf::Vector2f(0,0),Inputs *NewInput = 0);
			
			void Update();
			
			void PointTo(sf::Vector2f NewCenter);
			void SetTarget(Sprite *NewTarget);
			sf::Vector2f GetCoords();
	};
	
	#define CAMERA_INC
#endif