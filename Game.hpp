#ifndef GAME_INC
	#include <SFML/System.hpp>
	#include <SFML/Graphics.hpp>
	
	#include "Resources.hpp"
	
	enum ViewState{
		INTRO_STATE,
		PLAY_STATE,
		OH_NO_STATE,
		SAD_STATE
	};
	
	class Game{
		private:
			ViewState State;
			sf::RenderWindow Window;
			
			Game();
			
		public:
			static Game PP;
			
			Resources *Stuffs;
			sf::Time DeltaTime;
			
			sf::Vector2u ViewDimensions;
			
			void Run();
	};
	
	#define GAME_INC
#endif