#include "Button.hpp"

void Button::Update(){
	State = NORMAL_INPUT_STATE;
	
	for(std::list<Entity*>::iterator TT = CS->Entities.begin();TT != CS->Entities.end();++TT){
		if(*TT && (*TT)->GetEntityType() == SPRITE_ENTITY){
			Sprite *UT = static_cast<Sprite*>(*TT);
			
			if(UT->GetSpriteType() == PINKAMENA_SPRITE && IsTouching(UT)){
				if(CS->GetInputState(ButtonType) == PRESSED_INPUT_STATE){
					State = PRESSED_INPUT_STATE;
				}else{
					State = HOVER_INPUT_STATE;
				}
			}
		}
	}
	
	if(Target){
		Target->Input.Set(ButtonType,State);
	}
	
	/*if(BT->ButtonType > PLAYING_INPUT_TYPE_COUNT){
		if(BT->State == PRESSED_INPUT_STATE){
			BT->Redness = 1;
		}else{
			BT->Redness = (BT->State * 0.2 - BT->Redness) * 0.5 * Game::PP.DeltaTime.asSeconds();
		}
	}else if(BT->ButtonType < PLAYING_INPUT_TYPE_COUNT && BT->Target){
		BT->Target->Input.Set(BT->ButtonType,BT->State == PRESSED_INPUT_STATE);
	}*/
}

Button::Button(InputType NewType,sf::Vector2f NewPosition,CameraMonitor *NewTarget)
:Sprite(BUTTON_SPRITE,NewPosition),ButtonType(NewType),Target(NewTarget),State(NORMAL_INPUT_STATE),Redness(0){
	Z = -1;
	
	switch(ButtonType){
		case UP_INPUT:
			Display.setTexture(*Game::PP.Stuffs->RetrieveTexture(UP_BUTTON_TEXTURE));
			break;
		case DOWN_INPUT:
			Display.setTexture(*Game::PP.Stuffs->RetrieveTexture(DOWN_BUTTON_TEXTURE));
			break;
		case RIGHT_INPUT:
			Display.setTexture(*Game::PP.Stuffs->RetrieveTexture(RIGHT_BUTTON_TEXTURE));
			break;
		case LEFT_INPUT:
			Display.setTexture(*Game::PP.Stuffs->RetrieveTexture(LEFT_BUTTON_TEXTURE));
			break;
		case DOTHING_INPUT:
			Display.setTexture(*Game::PP.Stuffs->RetrieveTexture(DOTHING_BUTTON_TEXTURE));
			break;
		case CONTINUE_STORY_INPUT:
			Display.setTexture(*Game::PP.Stuffs->RetrieveTexture(CONTINUE_STORY_BUTTON_TEXTURE));
			break;
		case NEW_STORY_INPUT:
			Display.setTexture(*Game::PP.Stuffs->RetrieveTexture(NEW_STORY_BUTTON_TEXTURE));
			break;
		case NEW_SURVIVAL_INPUT:
			Display.setTexture(*Game::PP.Stuffs->RetrieveTexture(NEW_SURVIVAL_BUTTON_TEXTURE));
			break;
		case SURVIVAL_HIGHS_INPUT:
			Display.setTexture(*Game::PP.Stuffs->RetrieveTexture(SURVIVAL_HIGHS_BUTTON_TEXTURE));
			break;
		case MUTE_MUSIC_INPUT:
			Display.setTexture(*Game::PP.Stuffs->RetrieveTexture(MUTE_MUSIC_BUTTON_TEXTURE));
			break;
		case MUTE_AUDIO_INPUT:
			Display.setTexture(*Game::PP.Stuffs->RetrieveTexture(MUTE_AUDIO_BUTTON_TEXTURE));
			break;
		case INPUT_TYPE_COUNT:
			//Shut up, compiler warnings.
			break;
	}
	
	Display.setScale(1.0,(float)TILE_SIZE.y / TILE_SIZE.x);
}

void Button::SetTarget(CameraMonitor *NewTarget){
	Target = NewTarget;
}

sf::Vector2u Button::GetDimensions() const{
	switch(ButtonType){
		case UP_INPUT:
		case DOWN_INPUT:
		case RIGHT_INPUT:
		case LEFT_INPUT:
		case DOTHING_INPUT:
			
			break;
		case CONTINUE_STORY_INPUT:
		case NEW_STORY_INPUT:
		case NEW_SURVIVAL_INPUT:
		case SURVIVAL_HIGHS_INPUT:
			return sf::Vector2u(100,100.0 * Display.getScale().y);
			break;
		case MUTE_MUSIC_INPUT:
		case MUTE_AUDIO_INPUT:
			return sf::Vector2u(67,67.0 * Display.getScale().y);
			break;
		case INPUT_TYPE_COUNT:
			//Shut up, compiler warnings.
			break;
	}
	
	return sf::Vector2u(0,0);
}