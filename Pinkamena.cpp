#include "Pinkamena.hpp"

void Pinkamena::Update(){
	move(
		((CS->GetInputState(RIGHT_INPUT) == PRESSED_INPUT_STATE) - (CS->GetInputState(LEFT_INPUT) == PRESSED_INPUT_STATE)) * 150.f * Game::PP.DeltaTime.asSeconds(),
		((CS->GetInputState(DOWN_INPUT) == PRESSED_INPUT_STATE) - (CS->GetInputState(UP_INPUT) == PRESSED_INPUT_STATE)) * 150.f * Game::PP.DeltaTime.asSeconds()
	);
	
	sf::Vector2u PinkaPos(std::floor(getPosition().x / TILE_SIZE.x),std::floor(getPosition().y / TILE_SIZE.y));
	
	if(PinkaPos.y < CS->GetDimensions().y - 1){
		int CurrX = PinkaPos.x;
		
		while(CS->FadeTile(sf::Vector2u(CurrX,PinkaPos.y + 1))){
			++CurrX;
		}
		
		CurrX = PinkaPos.x;
		
		while(CS->FadeTile(sf::Vector2u(CurrX,PinkaPos.y + 1))){
			--CurrX;
		}
	}
	/*
	if((CS->Input.GetState(RIGHT_INPUT) == PRESSED_INPUT_STATE) - (CS->Input.GetState(LEFT_INPUT) == PRESSED_INPUT_STATE)){
		bool Flippage = (((CS->Input.GetState(RIGHT_INPUT) == PRESSED_INPUT_STATE) - (CS->Input.GetState(LEFT_INPUT) == PRESSED_INPUT_STATE)) + 1) * 0.5;
		
		if(Flippage != Flipped){
			Display.setOrigin(sf::Vector2f(40,115) + sf::Vector2f(Flippage * GetDimensions().x,0));
			Display.scale(1 - (Flippage * 2),1);
			
			Flipped = Flippage;
		}
	}*/
}

Pinkamena::Pinkamena(sf::Vector2f InitPosition)
:Sprite(PINKAMENA_SPRITE,InitPosition),Flipped(false){
	Display.setTexture(*Game::PP.Stuffs->RetrieveTexture(PONKA_TEXTURE));
	Display.setOrigin(sf::Vector2f(28,86));
}

sf::Vector2u Pinkamena::GetDimensions() const{
	return sf::Vector2u(61,3);
}