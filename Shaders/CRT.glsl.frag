uniform sampler2D CurrTex;

int ModInt = mod(gl_FragCoord.y,3);

void main(){
	gl_FragColor = texture2D(CurrTex,gl_TexCoord[0].xy) * vec4(float(ModInt == 0),float(ModInt == 1),float(ModInt == 2),1.0);
}