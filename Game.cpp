#include <cstdlib>

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

#include "Game.hpp"
#include "Resources.hpp"
#include "InputStuffs.hpp"
#include "Scene.hpp"
#include "TileMap.hpp"
#include "Camera.hpp"
#include "Entity.hpp"
#include "Sprite.hpp"
#include "CameraMonitor.hpp"
#include "Button.hpp"
#include "Pinkamena.hpp"

//#define SKIP_INTRO

Game Game::PP;

Game::Game()
#ifdef SKIP_INTRO
	:State(PLAY_STATE),ViewDimensions(sf::Vector2u(800,600)){
#else
	:State(INTRO_STATE),ViewDimensions(sf::Vector2u(800,600)){
#endif
}

void Game::Run(){
	bool DoThings = true;
	
	Window.create(sf::VideoMode::getDesktopMode(),"Puce Perished v0.1",sf::Style::Fullscreen);
	//Window.create(sf::VideoMode(800,600),"Puce Perished v0.05",sf::Style::Close);
	
	Stuffs = new Resources;
	
	sf::Clock DTimer,VTimer;
	
	//First scene.
	std::vector<int> *TestLevel = new std::vector<int>{
		 1, 1,48,49,50,51,52,53, 1, 1,
		13, 4,13, 4, 4, 4, 4,10, 4,10,
		 5, 8, 5, 0, 0, 0, 0, 2, 8, 2,
		 5, 8, 5, 0, 0, 0, 0, 2, 8, 2,
		 5, 8, 5, 0, 0, 0, 0, 2, 8, 2,
		12, 3,12, 3, 3, 3, 3,11, 3,11
	};
	
	std::set<int> Talls = {1,48,49,50,51,52,53};
	
	Scene One(sf::Vector2u(10,6));
	
	One.SetTallTiles(Talls);
	One.SetTiles(Stuffs->RetrieveTexture(TILES_TEXTURE_ATLAS),TestLevel);
	
	Pinkamena *Ponksa = new Pinkamena(sf::Vector2f(TILE_SIZE.x,TILE_SIZE.y));
	One.AddEntity(Ponksa);
	
	//Cameras
	CameraMonitor *Cameras = new CameraMonitor(sf::Vector2u(TILE_SIZE.x * 4,TILE_SIZE.y * 4),sf::Vector2f(TILE_SIZE.x * 3,TILE_SIZE.y * 1.5));
	One.AddEntity(Cameras);
	
	Button *Pressables[4]{
		new Button(CONTINUE_STORY_INPUT,sf::Vector2f(TILE_SIZE.x * 0.75,TILE_SIZE.y * 1.75),Cameras),
		new Button(NEW_STORY_INPUT,sf::Vector2f(TILE_SIZE.x * 0.75,TILE_SIZE.y * 4.25),Cameras),
		new Button(NEW_SURVIVAL_INPUT,sf::Vector2f(TILE_SIZE.x * 8.25,TILE_SIZE.y * 1.75),Cameras),
		new Button(SURVIVAL_HIGHS_INPUT,sf::Vector2f(TILE_SIZE.x * 8.25,TILE_SIZE.y * 4.25),Cameras)
	};
		
	for(int I = 0;I < 4;++I){
		One.AddEntity(Pressables[I]);
	}
	
	Camera Sight(Window.getSize(),&One);
	Sight.SetTarget(Ponksa);
	
	//Fancy out-of-scene rendering system.
	sf::RenderTexture View;
	View.create(Window.getSize().x,Window.getSize().y);
	
	sf::Sprite ViewSprite(View.getTexture());
	ViewSprite.setOrigin(Window.getSize().x * 0.5,Window.getSize().y * 0.5);
	
	#ifndef SKIP_INTRO
		ViewSprite.setScale(0,0);
	#endif
	
	sf::Sprite VoidObj(*Stuffs->RetrieveTexture(IRV_NAME_TEXTURE));
	VoidObj.setOrigin(Stuffs->RetrieveTexture(IRV_NAME_TEXTURE)->getSize().x * 0.5,Stuffs->RetrieveTexture(IRV_NAME_TEXTURE)->getSize().y * 0.5);
	VoidObj.setScale(5,5);
	//VoidObj.setColor(sf::Color(204,135,153,255));
	
	#ifdef SKIP_INTRO
		sf::View Eyes(sf::FloatRect(0,0,Window.getSize().x,Window.getSize().y));
	#else
		sf::View Eyes(sf::FloatRect(0,0,Window.getSize().x * 5,Window.getSize().y * 5));
	#endif
	
	Eyes.setCenter(sf::Vector2f(0,0));
	
	int LoadPhase = 0;
	float ScaleNorm = 0.001;
	
	while(Window.isOpen() && DoThings){
		sf::Event Event;
		
		while(Window.pollEvent(Event)){
			if(Event.type == sf::Event::Closed || (Event.type == sf::Event::KeyReleased && Event.key.code == sf::Keyboard::Escape)){
				DoThings = false;
			}
		}
		
		AllowSceneUpdasions();
		
		switch(State){
			case INTRO_STATE:
				if(State != 1 || VTimer.getElapsedTime().asSeconds() > 3){
					ScaleNorm *= 1 + DeltaTime.asSeconds() * 2;
				}
				
				ScaleNorm -= (ScaleNorm > 1) * (ScaleNorm - 1);
				
				if(LoadPhase == 2){
					ViewSprite.setScale(ScaleNorm,ScaleNorm);
					
					VoidObj.setScale(5 + 10 * ScaleNorm,5 + 10 * ScaleNorm);
					VoidObj.setColor(sf::Color(VoidObj.getColor().r,VoidObj.getColor().g,VoidObj.getColor().b,(1 - ScaleNorm) * 255));
					
					Eyes.setRotation(ScaleNorm * 360);
					Eyes.setSize(Window.getSize().x * (1 + ((1 - ScaleNorm) * 4)),Window.getSize().y * (1 + ((1 - ScaleNorm) * 4)));
				}else if(LoadPhase == 0){
					VoidObj.setColor(sf::Color(VoidObj.getColor().r,VoidObj.getColor().g,VoidObj.getColor().b,ScaleNorm * 255));
				}
				
				if(ScaleNorm >= 1){
					if(LoadPhase == 1){
						if(VTimer.getElapsedTime().asSeconds() > 3){
							ScaleNorm = 0.001;
							++LoadPhase;
						}
					}else{
						if(LoadPhase != 0){
							ScaleNorm = 0.001;
						}
						
						VTimer.restart();
						
						++LoadPhase;
					}
					
					if(LoadPhase == 3){
						State = PLAY_STATE;
					}
				}
				
				break;
			case PLAY_STATE:
				Sight.Input->Set(UP_INPUT,static_cast<InputState>((sf::Keyboard::isKeyPressed(sf::Keyboard::Up) || sf::Keyboard::isKeyPressed(sf::Keyboard::W)) * PRESSED_INPUT_STATE));
				Sight.Input->Set(DOWN_INPUT,static_cast<InputState>((sf::Keyboard::isKeyPressed(sf::Keyboard::Down) || sf::Keyboard::isKeyPressed(sf::Keyboard::S)) * PRESSED_INPUT_STATE));
				Sight.Input->Set(RIGHT_INPUT,static_cast<InputState>((sf::Keyboard::isKeyPressed(sf::Keyboard::Right) || sf::Keyboard::isKeyPressed(sf::Keyboard::D)) * PRESSED_INPUT_STATE));
				Sight.Input->Set(LEFT_INPUT,static_cast<InputState>((sf::Keyboard::isKeyPressed(sf::Keyboard::Left) || sf::Keyboard::isKeyPressed(sf::Keyboard::A)) * PRESSED_INPUT_STATE));
				Sight.Input->Set(DOTHING_INPUT,static_cast<InputState>((sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) * PRESSED_INPUT_STATE));
				
				break;
			case OH_NO_STATE:
				
				break;
			case SAD_STATE:
				
				break;
		}
		
		Sight.Update();
		One.Update();
		
		//Drawing it all
		Window.clear(sf::Color(0,0,0,255));
		
		View.clear(sf::Color(0,0,0,255));
		View.draw(Sight);
		View.display();
		
		Window.setView(Eyes);
		
		Window.draw(ViewSprite);
		
		if(State != PLAY_STATE){
			Window.draw(VoidObj);
		}
		
		Window.display();
		
		DeltaTime = DTimer.restart();
		
		if(DeltaTime > sf::seconds(0.25)){
			DeltaTime = sf::seconds(0.25);
		}
	}
	
	Window.close();
}

int main(){
	Game::PP.Run();
	
	return EXIT_SUCCESS;
}