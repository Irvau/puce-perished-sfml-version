#ifndef TALL_TILE_INC
	#include "Entity.hpp"
	
	class TallTile: public Entity{
		private:
			sf::VertexArray Vertices;
			sf::Texture *Texture;
			
			float Alpha;
			
			virtual void Update();
			virtual void draw(sf::RenderTarget& Target,sf::RenderStates States) const;
			
		public:
			bool Fade;
			
			TallTile(int TileID,sf::Vector2i NewPosition,sf::Texture *NewTexture = 0);
			
			bool SetTileID(int NewID);
			void Position(sf::Vector2i NewPosition);
			void SetTexture(sf::Texture *NewTexture);
			
			void SetAlpha(float NewAlpha);
			float GetAlpha();
			
			virtual sf::Vector2u GetDimensions() const;
	};
	
	#define TALL_TILE_INC
#endif