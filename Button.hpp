#ifndef BUTTON_INC
	#include <SFML/Graphics.hpp>
	
	#include "Game.hpp"
	#include "Sprite.hpp"
	#include "InputStuffs.hpp"
	#include "CameraMonitor.hpp"
	
	class Button: public Sprite{
		friend class Scene;	
		friend class Camera;
		
		private:
			InputType ButtonType;
			CameraMonitor *Target;
			
			InputState State;
			float Redness;
			
			virtual void Update();
			
		public:
			Button(InputType NewType,sf::Vector2f NewPosition,CameraMonitor *NewTarget = 0);
			
			void SetTarget(CameraMonitor *NewTarget);
			virtual sf::Vector2u GetDimensions() const;
	};
	
	#define BUTTON_INC
#endif