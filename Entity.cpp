#include <iostream>
#include <stdexcept>

#include <vector>

#include <SFML/Graphics.hpp>

#include "Game.hpp"
#include "Entity.hpp"

Entity::Entity(EntityType NewType,sf::Vector2f NewPosition)
:Type(NewType),CS(0),Z(0){
	setPosition(NewPosition);
}

bool Entity::IsTouching(Entity *OtherEntity){
	if(OtherEntity){
		return
			(getPosition().x + GetDimensions().x > OtherEntity->getPosition().x &&
			getPosition().x < OtherEntity->getPosition().x + OtherEntity->GetDimensions().x) &&
			(getPosition().y + GetDimensions().y > OtherEntity->getPosition().y &&
			getPosition().y < OtherEntity->getPosition().y + OtherEntity->GetDimensions().y)
		;
	}
	
	return false;
}

EntityType Entity::GetEntityType(){
	return Type;
}