#ifndef ENTITY_INC
	#include <list>
	
	#include <SFML/Graphics.hpp>
	
	#include "Scene.hpp"
	
	enum EntityType{
		TALL_TILE_ENTITY,
		SPRITE_ENTITY
	};
	
	class Entity: public sf::Transformable, public sf::Drawable{
		friend class Scene;
		friend class Camera;
		
		private:
			EntityType Type;
			
		protected:
			Scene *CS;
			int Z;
			
			virtual void Update() = 0;
			virtual void draw(sf::RenderTarget& Target,sf::RenderStates States) const = 0;
			
		public:
			Entity(EntityType NewType,sf::Vector2f NewPosition = sf::Vector2f(0,0));
			
			bool IsTouching(Entity *OtherEntity);
			
			EntityType GetEntityType();
			virtual sf::Vector2u GetDimensions() const = 0;
	};
	
	#define ENTITY_INC
#endif