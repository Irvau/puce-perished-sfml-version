#ifndef SPRITE_INC
	#include "Entity.hpp"
	
	enum SpriteType{
		PINKAMENA_SPRITE,
		MONITOR_SPRITE,
		BUTTON_SPRITE,
		
		SPRITE_TYPE_COUNT
	};
	
	class Sprite: public Entity{
		friend class Scene;
		friend class Camera;
		
		private:
			SpriteType Type;
			
		protected:
			sf::Sprite Display;
			
			virtual void Update() = 0;
			virtual void draw(sf::RenderTarget& Target,sf::RenderStates States) const;
			
		public:
			Sprite(SpriteType NewType,sf::Vector2f NewPosition = sf::Vector2f(0,0));
			
			SpriteType GetSpriteType();
			virtual sf::Vector2u GetDimensions() const = 0;
	};
	
	#define SPRITE_INC
#endif