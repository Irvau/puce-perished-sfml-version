#**"Puce Perished" (SFML Version)**#

A 2d top-down game involving a sad pink horse and a lot of pseudo 4th wall breaking.

**Current version:** v0.1

A thread for this game is located [here](http://mylittlegamedev.com/thread-1530.html), complete with compiled versions available for download, personal rantings of mine, and community feedback.

Please note that this version of Puce Perished is no longer being developed. You can find the newer version [here](https://bitbucket.org/Irvau/puce-perished).

##**Compilation Requirements**##

Make sure that your compiler is using the c++ 11 standard, link SFML, and the cpps should all compile fine.

Tested and confirmed working with SFML 2.4.1.