#include <iostream>
#include <limits>
#include <algorithm>
#include <list>

#include <SFML/System.hpp>

#include "Game.hpp"
#include "Resources.hpp"
#include "Scene.hpp"
#include "Entity.hpp"
#include "TallTile.hpp"

std::list<Scene*> Scenes;

void Scene::draw(sf::RenderTarget& Target,sf::RenderStates States) const{
	if(Tiles){
		Target.draw(*Tiles);
	}
	
	for(std::list<Entity*>::const_iterator IT = Entities.cbegin();IT != Entities.cend();++IT){
		if(*IT){
			Target.draw(**IT);
		}
	}
}

bool Scene::ZSortEntities(const Entity * const & A,const Entity * const & B){
	if(A->Z != B->Z){
		return A->Z < B->Z;
	}
	
	return (A->getPosition().y + A->GetDimensions().y) < (B->getPosition().y + B->GetDimensions().y);
}

Scene::Scene(sf::Vector2u NewDimensions)
:Dimensions(NewDimensions),Tiles(0),TallTiles(new TallTile*[Dimensions.x * Dimensions.y]),DunDidFrameUpdate(false){
	for(int I = 0;I < Dimensions.x * Dimensions.y;++I){
		TallTiles[I] = 0;
	}
	
	Scenes.push_back(this);
}

Scene::~Scene(){
	if(Tiles){
		delete Tiles;
	}
}

void Scene::SetTiles(sf::Texture *NewTexture,std::vector<int> *NewTiles){
	if(!Tiles){
		Tiles = new TileMap();
	}
	
	if(TallTiles){
		delete[] TallTiles;
	}
	
	Tiles->SetDimensions(Dimensions);
	Tiles->SetTexture(NewTexture);
	Tiles->SetTiles(NewTiles);
	
	TallTiles = new TallTile*[Dimensions.x * Dimensions.y];
	
	for(int I = 0;I < Dimensions.x * Dimensions.y;++I){
		TallTiles[I] = 0;
	}
	
	for(std::list<Entity*>::iterator IT = Entities.begin();IT != Entities.end();++IT){
		if((*IT)->Type == TALL_TILE_ENTITY){
			delete *IT;
			Entities.erase(IT);
		}
	}
	
	if(!TallTileIDs.empty()){
		for(int I = 0;I < Dimensions.x * Dimensions.y;++I){
			if(TallTileIDs.find((*NewTiles)[I]) != TallTileIDs.end()){
				TallTile *NewTallness = new TallTile((*NewTiles)[I],sf::Vector2i(I % Dimensions.x,std::floor(I / Dimensions.x)),Game::PP.Stuffs->RetrieveTexture(TILES_TEXTURE_ATLAS));
				
				TallTiles[I] = NewTallness;
				AddEntity(NewTallness);
			}
		}
	}
}

void Scene::SetTallTiles(std::set<int> NewTalls){
	TallTileIDs = NewTalls;
}

void Scene::AddEntity(Entity *NewEntity){
	if(NewEntity->CS){
		Entity *SceneEntity = *std::find(NewEntity->CS->Entities.begin(),NewEntity->CS->Entities.end(),NewEntity);
		
		if(SceneEntity){
			NewEntity->CS->Entities.remove(SceneEntity);
		}
	}
	
	NewEntity->CS = this;
	Entities.push_back(NewEntity);
}

void Scene::Update(){
	if(!DunDidFrameUpdate){
		//First the sprite updasions
		for(std::list<Entity*>::iterator IT = Entities.begin();IT != Entities.end();++IT){
			if((*IT)->GetEntityType() == SPRITE_ENTITY){
				(*IT)->Update();
			}
		}
		
		//Then the tall tile updasions.
		for(std::list<Entity*>::iterator IT = Entities.begin();IT != Entities.end();++IT){
			if((*IT)->GetEntityType() == TALL_TILE_ENTITY){
				(*IT)->Update();
			}
		}
		
		//And then collisions.
		for(std::list<Entity*>::iterator IT = Entities.begin();IT != Entities.end();++IT){
			if(*IT && (*IT)->Type == SPRITE_ENTITY){
				for(std::list<Entity*>::iterator JT = Entities.begin();JT != Entities.end();++JT){
					if(*JT && (*IT)->IsTouching(*JT) && (*JT)->Type == TALL_TILE_ENTITY){
						float Distances[] = {
							std::numeric_limits<float>::infinity(),
							(*IT)->getPosition().x + (*IT)->GetDimensions().x - (*JT)->getPosition().x,
							(*IT)->getPosition().y + (*IT)->GetDimensions().y - (*JT)->getPosition().y,
							(*IT)->getPosition().x - (*JT)->getPosition().x - (*JT)->GetDimensions().x,
							(*IT)->getPosition().y - (*JT)->getPosition().y - (*JT)->GetDimensions().y
						};
						
						int ClosestDistance = 0;
						
						for(int I = 1;I < 5;++I){
							if(std::abs(Distances[I]) < std::abs(Distances[ClosestDistance])){
								ClosestDistance = I;
							}
						}
						
						if(ClosestDistance){
							float Movement = -1 * Distances[ClosestDistance];
							
							if(ClosestDistance % 2){
								(*IT)->move(Movement,0);
							}else{
								(*IT)->move(0,Movement);
							}
						}
					}
				}
				
				//Collisions with scene boundaries.
				sf::Vector2f Limitation(
					(((*IT)->getPosition().x < 0) * (0 - (*IT)->getPosition().x)) +
					((((*IT)->getPosition().x + (*IT)->GetDimensions().x) > (Dimensions.x * TILE_SIZE.x)) * ((Dimensions.x * TILE_SIZE.x) - ((*IT)->getPosition().x + (*IT)->GetDimensions().x))),
					(((*IT)->getPosition().y < 0) * (0 - (*IT)->getPosition().y)) +
					((((*IT)->getPosition().y + (*IT)->GetDimensions().y) > (Dimensions.y * TILE_SIZE.y)) * ((Dimensions.y * TILE_SIZE.y) - ((*IT)->getPosition().y + (*IT)->GetDimensions().y)))
				);
				
				(*IT)->move(Limitation);
			}
		}
		
		//Now to order them.
		Entities.sort(ZSortEntities);
		
		DunDidFrameUpdate = true;
	}
}

void Scene::AllowUpdasion(){
	DunDidFrameUpdate = false;
	Input.Reset();
}

sf::Vector2u Scene::GetDimensions() const{
	return Dimensions;
}

InputState Scene::GetInputState(InputType Index) const{
	return Input.GetState(Index);
}

bool Scene::FadeTile(sf::Vector2u Coords){
	int Index = TileCoordToID(Coords);
	
	if(Index > Dimensions.x * Dimensions.y || !TallTiles[Index]){
		return false;
	}else{
		TallTiles[Index]->Fade = true;
	}
	
	return true;
}

void AllowSceneUpdasions(){
	for(std::list<Scene*>::iterator IT = Scenes.begin();IT != Scenes.end();++IT){
		(*IT)->AllowUpdasion();
	}
}