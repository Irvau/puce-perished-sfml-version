#include "Game.hpp"
#include "Resources.hpp"
#include "CameraMonitor.hpp"

void CameraMonitor::Update(){
	Texture.clear(sf::Color(0,0,0,255));
	
	for(std::list<Camera*>::iterator CT = Cameras.begin();CT != Cameras.end();++CT){
		(*CT)->Input->CopyFrom(&Input,false);
		(*CT)->Update();
	}
	
	for(std::list<Camera*>::iterator CT = Cameras.begin();CT != Cameras.end();++CT){
		if(*CT){
			if((*CT)->TargetScene){
				(*CT)->TargetScene->Update();
			}
			
			Texture.draw(**CT);
		}
	}
	
	InputType MostRecent;
	
	for(int I = 0;I < INPUT_TYPE_COUNT;++I){
		if(Input.GetState(static_cast<InputType>(I)) == HOVER_INPUT_STATE){
			MostRecent = static_cast<InputType>(I);
		}
	}
	
	if(OldInput != MostRecent){
		switch(MostRecent){
			case CONTINUE_STORY_INPUT:
				Message.setString("Continue Story");
				break;
			case NEW_STORY_INPUT:
				Message.setString("New Story");
				break;
			case NEW_SURVIVAL_INPUT:
				Message.setString("New Survival");
				break;
			case SURVIVAL_HIGHS_INPUT:
				Message.setString("Survival High Scores");
				break;
			case MUTE_MUSIC_INPUT:
				Message.setString("Mute Music");
				break;
			case MUTE_AUDIO_INPUT:
				Message.setString("Mute Audio");
				break;
			default:
				Message.setString("");
				break;
		}
		
		Message.setOrigin(Message.getLocalBounds().width * 0.5,Message.getLocalBounds().height * 0.5);
		
		OldInput = MostRecent;
	}
	
	Texture.draw(MessageBack);
	Texture.draw(Message);
	
	Texture.display();
	
	Snapples.setUniform("CurrTex",sf::Shader::CurrentTexture);
}

void CameraMonitor::draw(sf::RenderTarget& Target,sf::RenderStates States) const{
	States.transform *= getTransform();
	States.shader = &Snapples;
	
	Target.draw(Display,States);
}

CameraMonitor::CameraMonitor(sf::Vector2u NewDimensions,sf::Vector2f NewPosition)
:Sprite(MONITOR_SPRITE,NewPosition),Dimensions(NewDimensions),MessageBack(sf::Vector2f(Dimensions.x,Dimensions.y * 0.3)),Message("",*Game::PP.Stuffs->RetrieveFont(PUCEPERISHED_FONT)){
	Texture.create(std::ceil(Dimensions.x),std::ceil(Dimensions.y));
	Display.setTexture(Texture.getTexture());
	
	MessageBack.setPosition(0,Dimensions.y * 0.35);
	MessageBack.setFillColor(sf::Color(114,76,86));
	
	Message.setPosition(Dimensions.x * 0.5,Dimensions.y * 0.5);
	Message.setCharacterSize(std::floor(Dimensions.y * 0.15));
	
	Snapples.loadFromFile("Shaders/CRT.glsl.frag",sf::Shader::Type::Fragment);
	//Snapples.loadFromFile("Shaders/TexCoord.glsl.vert","Shaders/CRT.glsl.frag");
	
	Z = -2;
}

sf::Vector2u CameraMonitor::GetDimensions() const{
	return Dimensions;
}

Camera* CameraMonitor::AddCamera(Scene* NewTargetScene,int NewID,sf::Vector2f InitPosition){
	Camera *NewCamera = new Camera(Dimensions,NewTargetScene,NewID,InitPosition,&Input);
	Cameras.push_back(NewCamera);
	
	return NewCamera;
}

Camera* CameraMonitor::GetCamera(int TargetID){
	Camera *ReturnCamera = 0;
	
	for(std::list<Camera*>::iterator IT = Cameras.begin();IT != Cameras.end();++IT){
		if((*IT)->ID == TargetID){
			ReturnCamera = *IT;
		}
	}
	
	return ReturnCamera;
}

void CameraMonitor::SetCameraSize(int TargetID,sf::Vector2f NewSize){
	Camera *AffectedCamera = GetCamera(TargetID);
	
	if(AffectedCamera){
		AffectedCamera->View.setSize(NewSize);
	}
}

void CameraMonitor::SetCameraViewport(int TargetID,sf::FloatRect NewViewport){
	Camera *AffectedCamera = GetCamera(TargetID);
	
	if(AffectedCamera){
		AffectedCamera->View.setViewport(NewViewport);
	}
}