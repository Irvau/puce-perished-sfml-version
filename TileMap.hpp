#ifndef TILEMAP_INC
	#include <vector>
	
	#include <SFML/Graphics.hpp>
	
	const sf::Vector2i TILE_SIZE(100,75),TILE_TEXTURE_SIZE(32,32);
	
	class TileMap: public sf::Transformable, public sf::Drawable{
		private:
			sf::Vector2u Dimensions;
			std::vector<int>Tiles;
			
			sf::Texture *Texture;
			sf::VertexArray Vertices;
			
			virtual void draw(sf::RenderTarget& Target,sf::RenderStates States) const;
			
		public:
			TileMap(sf::Vector2u NewDimensions = sf::Vector2u(0,0),sf::Texture *NewTexture = 0,std::vector<int> *NewTiles = 0);
			
			void SetDimensions(sf::Vector2u NewDimensions);
			void SetTexture(sf::Texture *NewTexture);
			bool SetTiles(std::vector<int> *NewTiles);
	};
	
	#define TILEMAP_INC
#endif