#ifndef SCENE_INC
	#include <cmath>
	#include <list>
	#include <set>
	
	#include <SFML/System.hpp>
	#include <SFML/Graphics.hpp>
	
	#include "InputStuffs.hpp"
	#include "TileMap.hpp"
	
	class Entity;
	class Sprite;
	class TallTile;
	
	class Scene: public sf::Drawable{
		friend class Game;
		friend class Camera;
		
		private:
			Inputs Input;
			
			sf::Vector2u Dimensions;
			
			TileMap *Tiles;
			std::set<int> TallTileIDs;
			
			bool DunDidFrameUpdate;
			
			virtual void draw(sf::RenderTarget& Target,sf::RenderStates States) const;
			
			static bool ZSortEntities(const Entity * const & A,const Entity * const & B);
			
		public:
			std::list<Entity*> Entities;
			TallTile **TallTiles;
			
			Scene(sf::Vector2u NewDimensions = sf::Vector2u(0,0));
			~Scene();
			
			void SetTiles(sf::Texture *NewTexture,std::vector<int> *NewTiles = 0);
			void SetTallTiles(std::set<int> NewTalls);
			
			void AddEntity(Entity *NewEntity);
			
			void Update();
			void AllowUpdasion();
			
			sf::Vector2u GetDimensions() const;
			InputState GetInputState(InputType Index) const;
			
			bool FadeTile(sf::Vector2u Coords);
			
			int TileCoordToID(sf::Vector2u TileCoord){
				return Dimensions.x * TileCoord.y + TileCoord.x;
			}
			
			sf::Vector2u IDToTileCoord(int ID){
				return sf::Vector2u(ID % Dimensions.x,std::floor(ID / Dimensions.y));
			}
	};
	
	void AllowSceneUpdasions();
	
	#define SCENE_INC
#endif