#ifndef CAMERA_MONITOR_INC
	#include <list>
	
	#include <SFML/Graphics.hpp>
	
	#include "Sprite.hpp"
	#include "InputStuffs.hpp"
	#include "Camera.hpp"
	
	class CameraMonitor: public Sprite{
		friend class Scene;
		friend class Button;
		
		private:
			Inputs Input;
			InputType OldInput;
			
			sf::Vector2u Dimensions;
			
			std::list<Camera*> Cameras;
			sf::RectangleShape MessageBack;
			sf::Text Message;
			
			sf::RenderTexture Texture;
			
			sf::Shader Snapples;
			
			virtual void Update();
			virtual void draw(sf::RenderTarget& Target,sf::RenderStates States) const;
			
		public:
			CameraMonitor(sf::Vector2u NewDimensions,sf::Vector2f NewPosition = sf::Vector2f(0,0));
			
			virtual sf::Vector2u GetDimensions() const;
			
			Camera* AddCamera(Scene* NewTargetScene = 0,int NewID = 0,sf::Vector2f InitPosition = sf::Vector2f(0,0));
			Camera* GetCamera(int TargetID);
			
			void SetCameraSize(int TargetID,sf::Vector2f NewSize);
			void SetCameraViewport(int TargetID,sf::FloatRect NewViewport);
	};
	
	#define CAMERA_MONITOR_INC
#endif