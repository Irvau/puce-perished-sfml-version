#include "Game.hpp"
#include "Sprite.hpp"

void Sprite::draw(sf::RenderTarget& Target,sf::RenderStates States) const{
	if(Display.getTexture()){
		States.transform *= getTransform();
		
		Target.draw(Display,States);
	}
}

Sprite::Sprite(SpriteType NewType,sf::Vector2f NewPosition)
:Entity(SPRITE_ENTITY,NewPosition),Type(NewType){
	
}

SpriteType Sprite::GetSpriteType(){
	return Type;
}